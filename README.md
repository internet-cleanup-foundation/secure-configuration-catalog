# Secure Configuration Catalog
Copy paste examples for making your platform safer.

This is for internet products that are shipped insecure by default. For example all products that use server banners. Use these example configuration to make them as secure as possible.

If you find information from this catalog helpful we ask two things in return:

1. try to contribute your configuration knowledge for specific products, see below
2. tell your vendor (again...) that their product should be secure by default / create a pull request to improve security by default


Inspiration for this project comes from cipherli.st, which does this for setting up TLS in a wide range of products.

You can find example configurations in this github repository, sorted by vendor and product. You can use the search function to find a config based on: vendor, category, product type and such.

Our goal is that these products ship with this configuration by default: so you should not need these configurations anymore in the future.

Here you can find standard configurations for:
* Web Server Security Headers, limiting permissions, respecting privacy and  security
* Eliminating Service Banners
* Empty error pages on web servers (or redirects on errors)
* Setting up TLS (cipherli.st) + more
* Setting up Firewalls (based on Clean IT surface)


## When to add configs

The ideal moment to add new / alternative secure configurations is _after_ a succesful IT security audit. When your stuff is 'okayed' you can add (variants) of secure configurations to the repository: this will help other with similar products tremendously. Especially for more exclusive products like expensive firewalls and such (with poor documentation etc) your shared configuration will save hundreds of hours of research done by others.
